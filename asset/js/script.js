const API_DOMAIN = "http://192.168.0.12:8000/api/";
const WEBAPP_DOMAIN = "http://localhost/webapp/";
const WEBSOCKET_DOMAIN = "http://localhost:6001/";

$("#login").click(function() {
  var tab = new FormData();
  email = $("#email_login").val();
  password = $("#password_login").val();
  BASE_URL = "http://localhost/webapp/";

  tab.append("email", email);
  tab.append("password", password);

  $.ajax({
    url: API_DOMAIN + "login",
    type: "POST",
    data: tab,
    processData: false,
    contentType: false,
    dataType: "json",
    success: function(res) {
      if (res.error) {
          alert("Mauvais identifiants");
      } else {
        localStorage.setItem("email", res["user"]["email"]);
        localStorage.setItem("lastname", res["user"]["lastname"]);
        localStorage.setItem("firstname", res["user"]["firstname"]);
        localStorage.setItem("token", res["success"]["token"]);
        localStorage.setItem("courses", JSON.stringify(res["user"]["courses"]));
        window.location = WEBAPP_DOMAIN + "main.html";
      }
    },
    error: function(error) {
      console.error(error);
      alert("Erreur lors de la connexion avec le serveur");
    }
  });
});

$("#register").click(function() {
  let form_data = new FormData();
  firstname = $("#firstname").val();
  lastname = $("#lastname").val();
  email = $("#email_register").val();
  password = $("#password_register").val();
  confirm_password = $("#password_confirm").val();

  form_data.append("firstname", firstname);
  form_data.append("lastname", lastname);
  form_data.append("email", email);
  form_data.append("password", password);
  form_data.append("confirm_password", confirm_password);
  form_data.append("admin", 1);

  $.ajax({
    url: API_DOMAIN + "register",
    type: "POST",
    data: form_data,
    processData: false,
    contentType: false,
    dataType: "json",
    success: function(res) {
        localStorage.setItem("email", res["user"]["email"]);
        localStorage.setItem("lastname", res["user"]["lastname"]);
        localStorage.setItem("firstname", res["user"]["firstname"]);
        localStorage.setItem("token", res["success"]["token"]);
        window.location = WEBAPP_DOMAIN + "main.html";
    },
    error: function(error) {
      console.log(error);
      alert("Perdu");
    }
  });
});

function logout() {
  localStorage.clear();
  window.location = WEBAPP_DOMAIN;
}

$(document).ready(function() {
  //Link for example: https://laracasts.com/discuss/channels/laravel/using-laravel-broadcasting-without-laravel-echo?#reply=405136
  // var socket = io.connect('http://localhost:6001');
  // socket.emit('subscribe', {
  //     channel: 'laravel_database_test',
  // }).on('App\\Events\\NewMessage', function(channel, data){
  //     console.log("Channel", channel);
  //     console.log("Data", data);
  // })

  // $('#classes').select2({
  //     placeholder : "test"
  // });

  $("#connect").on("click", function() {
    $("#inscription").hide();
    $("#connection").show();
  });

  $("#inscrire").on("click", function() {
    $("#inscription").show();
    $("#connection").hide();
  });
});
